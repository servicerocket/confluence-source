/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package org.randombits.source.confluence;

import org.randombits.source.BaseSource;
import org.randombits.source.SourceException;

import java.io.Reader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.io.OutputStream;
import java.io.InputStreamReader;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentEntityManager;
import bucket.container.ContainerManager;

/**
 * Provides access to an attachment.
 *
 * @author David Peterson
 */
public class AttachmentSource extends BaseSource
{
    private long contentId;

    private String fileName;

    private transient ContentEntityObject _content;

    private transient ContentEntityManager contentEntityManager;
    private transient AttachmentManager attachmentManager;

    public AttachmentSource(Attachment attachment)
    {
        this(attachment.getContent(), attachment.getFileName());
    }

    public AttachmentSource(ContentEntityObject content, String fileName)
    {
        setContent(content);
        setFileName(fileName);
    }

    public AttachmentSource()
    {}

    protected void checkPreparation() throws SourceException
    {
        if (contentId <= 0)
            throw new SourceException("The content object has not been specified.");

        if (fileName == null)
            throw new SourceException("The filename of the attachment has not been specified.");
    }

    protected Reader createReader() throws IOException, SourceException
    {
        return new InputStreamReader(createInputStream());
    }

    protected InputStream createInputStream() throws IOException, SourceException
    {
        return getAttachment().getContentsAsStream();
    }

    protected Writer createWriter() throws IOException, SourceException
    {
        throw new UnsupportedOperationException("Attachment sources may not be written to.");
    }

    protected OutputStream createOutputStream() throws IOException, SourceException
    {
        throw new UnsupportedOperationException("Attachment sources may not be written to.");
    }

    public boolean canRead()
    {
        try
        {
            return getAttachment() != null;
        }
        catch (SourceException e)
        {
            return false;
        }
    }

    public boolean canWrite()
    {
        return false;
    }

    private Attachment getAttachment() throws SourceException
    {
        return getContent().getAttachmentNamed(fileName);
    }

    private ContentEntityObject getContent() throws SourceException
    {
        if (_content == null)
            _content = getContentEntityManager().getById(contentId);

        if (_content == null)
            throw new SourceException("Unable to locate content with id: " + contentId);

        return _content;
    }

    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    public void setAttachmentManager(AttachmentManager attachmentManager)
    {
        this.attachmentManager = attachmentManager;
    }

    protected ContentEntityManager getContentEntityManager()
    {
        if (contentEntityManager == null)
            contentEntityManager = (ContentEntityManager) ContainerManager.getComponent("contentEntityManager");
        return contentEntityManager;
    }

    protected AttachmentManager getAttachmentManager()
    {
        if (attachmentManager == null)
            attachmentManager = (AttachmentManager) ContainerManager.getComponent("attachmentManager");
        return attachmentManager;
    }


    private void setContent(ContentEntityObject content)
    {
        setContentId(content.getId());
    }

    public void setContentId(long contentId)
    {
        if (this.contentId != contentId)
        {
            this.contentId = contentId;
            _content = null;
        }
    }

    public void setFileName(String fileName)
    {
        this.fileName = fileName;
    }

    public long getContentId()
    {
        return contentId;
    }

    public String getFileName()
    {
        return fileName;
    }
}
