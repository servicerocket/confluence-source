/*
 * Copyright (c) 2006, David Peterson
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "randombits.org" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.source.confluence;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentPropertyManager;

import java.io.*;

import bucket.container.ContainerManager;
import org.randombits.source.BaseSource;
import org.randombits.source.SourceException;

/**
 * This Source implementation reads and writes from the specified content property value.
 */
public class ContentPropertySource extends BaseSource
{
    private long contentId;
    private String propertyName;

    private transient ContentEntityObject _content;

    private transient ContentEntityManager contentEntityManager;
    private transient ContentPropertyManager contentPropertyManager;

    public ContentPropertySource(ContentEntityObject content, String propertyName)
    {
        this(content.getId(), propertyName);
    }

    public ContentPropertySource(long contentId, String propertyName)
    {
        this();
        setPropertyName(propertyName);
        setContentId(contentId);
    }

    public ContentPropertySource()
    {
        ContainerManager.autowireComponent(this);
    }

    public boolean isPrepared()
    {
        return propertyName != null && getContent() != null;
    }

    protected void checkPreparation() throws SourceException
    {
        if (getContent() == null)
            throw new SourceException("Could not find content for specified ID: " + contentId);

        if (propertyName == null || propertyName.length() == 0)
            throw new SourceException("Please set a property name");
    }

    public boolean canRead()
    {
        try
        {
            return getPropertyValue() != null;
        }
        catch (SourceException e)
        {
            return false;
        }
    }

    public Reader createReader() throws IOException, SourceException
    {
        String value = getPropertyValue();

        return new StringReader(value);
    }

    private String getPropertyValue()
            throws SourceException
    {
        ContentEntityObject content = getContent();
        if (content == null)
            throw new SourceException("No content has been specified to load the _group from.");

        if (propertyName == null)
            throw new SourceException("No property has been specified to load the _group from.");

        return contentPropertyManager.getTextProperty(getContent(), propertyName);
    }

    public InputStream createInputStream() throws IOException, SourceException
    {
        String value = getPropertyValue();

        return new ByteArrayInputStream(value.getBytes("UTF-8"));
    }

    public boolean canWrite()
    {
        return true;
    }

    public Writer createWriter() throws IOException, SourceException
    {
        final ContentEntityObject content = getContent();
        if (content == null)
            throw new SourceException("No content has been specified to load the _group from.");

        if (propertyName == null)
            throw new SourceException("No property has been specified to load the _group from.");

        final StringWriter writer = new StringWriter();
        return new FilterWriter(writer)
        {
            public void flush() throws IOException
            {
                super.flush();
                contentPropertyManager.setTextProperty(content, propertyName, writer.toString());
            }

            public void close() throws IOException
            {
                super.close();
                contentPropertyManager.setTextProperty(content, propertyName, writer.toString());
            }
        };
    }

    public OutputStream createOutputStream() throws IOException, SourceException
    {
        final ContentEntityObject content = getContent();
        if (content == null)
            throw new SourceException("No content has been specified to load the _group from.");

        if (propertyName == null)
            throw new SourceException("No property has been specified to load the _group from.");

        final ByteArrayOutputStream output = new ByteArrayOutputStream();

        return new FilterOutputStream(output)
        {
            public void flush() throws IOException
            {
                super.flush();
                contentPropertyManager.setTextProperty(content, propertyName, output.toString("UTF-8"));
            }

            public void close() throws IOException
            {
                super.close();
                contentPropertyManager.setTextProperty(content, propertyName, output.toString("UTF-8"));
            }
        };
    }

    public long getContentId()
    {
        return contentId;
    }

    public void setContentId(long contentId)
    {
        this.contentId = contentId;
        this._content = null;
    }

    public ContentEntityObject getContent()
    {
        if (_content == null && contentId != 0)
        {
            _content = contentEntityManager.getById(contentId);
        }
        return _content;
    }

    public void setContent(ContentEntityObject content)
    {
        _content = content;
        if (content != null)
            contentId = content.getId();
        else
            contentId = 0;
    }

    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public String getPropertyName()
    {
        return propertyName;
    }

    public void setPropertyName(String propertyName)
    {
        this.propertyName = propertyName;
    }
}
