/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.source;

import java.io.*;

/**
 * This is the root interface for objects which act as IO Sources.
 * Any classes which implement this interface must be {@link Serializable},
 * so make sure any local fields which should not be stored are marked
 * <code>transient</code>.
 */
public interface Source extends Serializable
{
    /**
     * This method returns <code>true</code> if the source has been
     * set up and is ready to start reading/writing/deleting.
     *
     * @return <code>true</code> if this source is ready for operations.
     */
    boolean isPrepared();

    /**
     * Checks if the {@link #openReader()} and/or {@link #openInputStream()} methods should work
     * (ie the source can be read). In some cases, sources can
     * never be read. In others, they need to have something written before
     * they can be read. In both cases, this method will return <code>false</code>.
     * @return <code>true</code> if the source can be read.
     */
    boolean canRead();

    /**
     * Returns a reader connected to the source.
     * <p>
     * <b>Note:</b> It is a Bad Idea to try opening more than one Reader or InputStream simultaneously.
     *
     * @return The opened Reader instance.
     * @throws IOException If there was an IO problem while connecting.
     * @throws SourceException If there was some other problem.
     * @throws UnsupportedOperationException if the source cannot be read.
     * @see #canRead()
     */
    Reader openReader() throws IOException, SourceException;

    /**
     * Returns an output stream connected to the source.
     *
     * @return
     * @throws IOException if there was an IO problem while connecting.
     * @throws SourceException if there was some other problem.
     * @throws UnsupportedOperationException if the source cannot be read.
     * @see #canRead()
     */
    InputStream openInputStream() throws IOException, SourceException;

    /**
     * Checks if the {@link #openWriter()} and/or {@link #openOutputStream()}
     * methods are legal (ie. the source can be written to). In some cases
     * sources can never be written to.
     *
     * @return <code>true</code> if the source can be written to.
     */
    boolean canWrite();

    Writer openWriter() throws IOException, SourceException;

    OutputStream openOutputStream() throws IOException, SourceException;
}
