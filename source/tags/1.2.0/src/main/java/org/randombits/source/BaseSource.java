/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.source;

import java.io.*;

/**
 * Abstract implementation of Source which provides a framwork
 * to make implementation of concrete Sources easier.
 * 
 * @author David Peterson
 */
public abstract class BaseSource implements Source
{
    public boolean isPrepared()
    {
        try
        {
            checkPreparation();
            return true;
        }
        catch (SourceException e)
        {
            return false;
        }
    }

    protected abstract void checkPreparation() throws SourceException;

    public Reader openReader() throws IOException, SourceException
    {
        if (!canRead())
            throw new UnsupportedOperationException("This source cannot be read from.");

        checkPreparation();

        return createReader();
    }

    protected abstract Reader createReader() throws IOException, SourceException;

    public InputStream openInputStream() throws IOException, SourceException
    {
        if (!canRead())
            throw new UnsupportedOperationException("This source cannot be read from.");

        checkPreparation();

        return createInputStream();
    }

    protected abstract InputStream createInputStream() throws IOException, SourceException;

    public Writer openWriter() throws IOException, SourceException
    {
        if (!canWrite())
            throw new UnsupportedOperationException("This source cannot be written to.");

        checkPreparation();

        return createWriter();
    }

    protected abstract Writer createWriter() throws IOException, SourceException;

    public OutputStream openOutputStream() throws IOException, SourceException
    {
        if (!canWrite())
            throw new UnsupportedOperationException("This source cannot be written to.");

        checkPreparation();

        return createOutputStream();
    }

    protected abstract OutputStream createOutputStream() throws IOException, SourceException;
}
