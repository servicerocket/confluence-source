/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.source;

import java.io.*;

/**
 * Stores its contents as a local string value.
 */
public class StringSource extends BaseSource
{
    private String value;

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    protected void checkPreparation() throws SourceException
    {
    }

    public boolean canRead()
    {
        return value != null;
    }

    public boolean canWrite()
    {
        return true;
    }

    @Override
    protected Reader createReader() throws IOException, SourceException
    {
        return new StringReader(value);
    }

    @Override
    protected InputStream createInputStream() throws IOException, SourceException
    {
        return new ByteArrayInputStream(value.getBytes("UTF-8"));
    }

    @Override
    protected Writer createWriter() throws IOException, SourceException
    {
        final StringWriter writer = new StringWriter();
        return new FilterWriter(writer)
        {
            @Override
            public void flush() throws IOException
            {
                super.flush();
                value = writer.toString();
            }

            @Override
            public void close() throws IOException
            {
                super.close();
                value = writer.toString();
            }
        };
    }

    @Override
    protected OutputStream createOutputStream() throws IOException, SourceException
    {
        final ByteArrayOutputStream output = new ByteArrayOutputStream();

        return new FilterOutputStream(output)
        {
            @Override
            public void flush() throws IOException
            {
                value = output.toString("UTF-8");
            }

            @Override
            public void close() throws IOException
            {
                super.close();
                value = output.toString("UTF-8");
            }
        };
    }
}
