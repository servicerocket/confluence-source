/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.randombits.source;

import org.apache.commons.codec.binary.Base64;

import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * Connects to a URL to retrieve the source value. This is read-only.
 */
public class URLSource extends BaseSource {
    private static final String WWW_AUTHENTICATE = "WWW-Authenticate";

    /**
     * The default timeout, in milliseconds. The default value of 0 indicates
     * that no timeout will be set.
     */
    private static final int DEFAULT_TIMEOUT = 0;

    private URL url;

    private String username;

    private String password;

    private Charset encoding;

    private int connectionTimeout = DEFAULT_TIMEOUT;

    private int readTimeout = DEFAULT_TIMEOUT;

    public URLSource( URL url ) {
        this( url, null, null );
    }

    public URLSource( URL url, String username, String password ) {
        this( url, username, password, null );
    }

    public URLSource( URL url, String username, String password, Charset encoding ) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.encoding = encoding;
    }

    public URLSource() {
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl( URL url ) {
        this.url = url;
    }

    @Override
    protected void checkPreparation() throws SourceException {
        if ( url == null )
            throw new SourceException( "Please specify a URL" );
    }

    public boolean canRead() {
        return true;
    }

    public boolean canWrite() {
        return false;
    }

    @Override
    public Reader createReader() throws IOException, SourceException {
        URLConnection conn = openConnection();

        Charset thisEnc = encoding;

        if ( thisEnc == null && conn instanceof HttpURLConnection ) {
            HttpURLConnection httpConn = ( HttpURLConnection ) conn;
            String contentTypeString = httpConn.getContentType();
            if ( contentTypeString != null ) {
                try {
                    ContentType contentType = new ContentType( contentTypeString );
                    String charset = contentType.getParameter( "charset" );

                    if ( charset != null )
                        thisEnc = Charset.forName( charset );
                } catch ( ParseException e ) {
                    e.printStackTrace();
                }
            }
        }

        return new UnicodeReader( createInputStream( conn ), thisEnc );
    }

    private URLConnection openConnection() throws IOException {
        URLConnection conn = url.openConnection();

        if ( connectionTimeout != DEFAULT_TIMEOUT )
            conn.setConnectTimeout( connectionTimeout );
        if ( readTimeout != DEFAULT_TIMEOUT )
            conn.setReadTimeout( readTimeout );

        conn.connect();
        return conn;
    }

    @Override
    public InputStream createInputStream() throws IOException, SourceException {
        URLConnection conn = openConnection();

        return createInputStream( conn );
    }

    private InputStream createInputStream( URLConnection conn ) throws SourceException, IOException {
        InputStream in;
        if ( connectionTimeout != DEFAULT_TIMEOUT ) {
            conn.setConnectTimeout( connectionTimeout );
        }

        if ( conn instanceof HttpURLConnection )
            in = httpInputStream( ( HttpURLConnection ) conn );
        else
            in = conn.getInputStream();

        return new BufferedInputStream( in );
    }

    private InputStream httpInputStream( HttpURLConnection conn ) throws SourceException, IOException {
        switch ( conn.getResponseCode() ) {
            case HttpURLConnection.HTTP_OK:
                return conn.getInputStream();
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                String wwwAuthenticate = conn.getHeaderField( WWW_AUTHENTICATE );
                if ( wwwAuthenticate != null ) {
                    if ( wwwAuthenticate.toLowerCase().startsWith( "basic " ) )
                        return basicInputStream();
                    else
                        throw new SourceException( "Unsupported authentication method required." );
                }
            default:
                throw new SourceException( conn.getResponseMessage() + " (" + conn.getResponseCode() + ")" );
        }
    }

    private InputStream basicInputStream() throws IOException, SourceException {
        if ( username == null )
            throw new SourceException( "A username is required" );

        HttpURLConnection conn = ( HttpURLConnection ) openConnection();

        String auth = "Basic ";

        String userpass = username + ":" + getPassword();
        auth += encodeBase64( userpass );

        conn.addRequestProperty( "Authorization", auth );

        conn.connect();
        switch ( conn.getResponseCode() ) {
            case HttpURLConnection.HTTP_OK:
                return conn.getInputStream();
            case HttpURLConnection.HTTP_UNAUTHORIZED:
                throw new SourceException( "Username and/or password not accepted." );
            default:
                String message = conn.getHeaderField( "x-ical-message" );
                if ( message == null )
                    message = conn.getResponseMessage() + " (" + conn.getResponseCode() + ")";
                throw new SourceException( message );
        }
    }

    private String encodeBase64( String string ) throws UnsupportedEncodingException {
        return new String( Base64.encodeBase64( string.getBytes( "iso-8859-1" ) ) );
    }

    private String decodeBase64( String string ) throws UnsupportedEncodingException {
        return new String( Base64.decodeBase64( string.getBytes( "iso-8859-1" ) ) );
    }

    @Override
    protected Writer createWriter() throws IOException, SourceException {
        return null;
    }

    @Override
    protected OutputStream createOutputStream() throws IOException, SourceException {
        return null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername( String username ) {
        this.username = username;
    }

    private String getPassword() {
        try {
            return ( password == null ) ? "" : decodeBase64( password );
        } catch ( UnsupportedEncodingException e ) {
            e.printStackTrace();
            return null;
        }
    }

    public void setPassword( String password ) {
        try {
            this.password = ( password == null ) ? null : encodeBase64( password );
        } catch ( UnsupportedEncodingException e ) {
            e.printStackTrace();
        }
    }

    public boolean hasPassword() {
        return password != null;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public void setEncoding( Charset encoding ) {
        this.encoding = encoding;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    /**
     * The connection timeout value in milliseconds. Must be non negative or an
     * exception will be thrown. A zero value means the default infinite timeout
     * will be used.
     * 
     * @param connectionTimeout The new connection timeout value.
     */
    public void setConnectionTimeout( int connectionTimeout ) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    /**
     * The read timeout value in milliseconds. Must be non-negative or an
     * exception will be thrown when connecting. A zero value means the default
     * infinite timeout will be used.
     * 
     * @param readTimeout The new read timeout value.
     */
    public void setReadTimeout( int readTimeout ) {
        this.readTimeout = readTimeout;
    }
}
